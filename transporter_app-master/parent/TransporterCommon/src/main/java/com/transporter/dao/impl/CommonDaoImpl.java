package com.transporter.dao.impl;

import org.springframework.stereotype.Repository;

import com.transporter.dao.CommonDao;

/**
 * @author Devappa.Arali
 *
 */

@Repository
public class CommonDaoImpl extends GenericDaoImpl implements CommonDao {

}
