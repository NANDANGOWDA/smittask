package com.transporter.constants;

public class WebConstants {
	// Rest messages
	public static final String FAILURE = "Failure";
	public static final String INTERNAL_SERVER_ERROR_CODE = "500";
	public static final String INTERNAL_SERVER_ERROR_MESSAGE = "internal server error";

	public static final String WEB_RESPONSE_SUCCESS = "SUCCESS";
	public static final String WEB_RESPONSE_FAILURE = "FAILURE";
	public static final String WEB_RESPONSE_ERROR = "ERROR";
	public static final String WEB_RESPONSE_NO_RECORD_FOUND = "DATA_DOES_NOT_EXIST";
	public static final String SUCCESS = "success";
	public static final String INVALID_USER = "Invalid user";
	public static final String NO_DATA_FOUND = "No data found";
	public static final String NOT_UPDATED = "Data not updated";
	public static final String VEHICLE_NOT_UPDATED = "Vehicle Not Found";
	public static final String VEHICLE_TYPE_NOT_SAVED ="Vehicle type not saved";
	public static final String VEHICLE_TYPE_NOT_UPDATED ="Vehicle type not updated";
	public static final String VEHICLES_NOT_AVAILABLE = "Vehicles not available";
	public static final String INVALID_STATUS = "Enter Valid Status Value";
	public static final String INVALID_TRIP_ID = "Invalid Trip ID";
	
}
