package com.transporter.vo;

/**
 * @author Devappa.Arali
 *
 */

public class GoodsTypeVo {
	private int id;
	private String goodsType;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGoodsType() {
		return goodsType;
	}
	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}
}
