package com.transporter.response;

public class LatitudeLongitudeResponse  {
	
	
	
	private Double currentLattitude;
	
	private Double currentLongitude;

	public Double getCurrentLattitude() {
		return currentLattitude;
	}

	public void setCurrentLattitude(Double currentLattitude) {
		this.currentLattitude = currentLattitude;
	}

	public Double getCurrentLongitude() {
		return currentLongitude;
	}

	public void setCurrentLongitude(Double currentLongitude) {
		this.currentLongitude = currentLongitude;
	}

}
