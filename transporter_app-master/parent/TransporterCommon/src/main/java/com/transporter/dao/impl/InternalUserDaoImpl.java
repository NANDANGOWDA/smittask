package com.transporter.dao.impl;

import org.springframework.stereotype.Repository;

import com.transporter.dao.InternalUserDao;

/**
 * @author Devappa.Arali
 *
 */

@Repository
public class InternalUserDaoImpl extends GenericDaoImpl implements InternalUserDao {

}
