package com.transporter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.transporter.vo.GoodsTypeVo;

@Entity
@Table(name = "goodstype")
public class GoodsType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "goods_type")
	private String goodsType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}
	
	public static GoodsTypeVo convertModelToVo(GoodsType goodsType) {
		if(null == goodsType)
			return null;
		GoodsTypeVo goodsTypeVo = new GoodsTypeVo();
		goodsTypeVo.setId(goodsType.getId());
		goodsTypeVo.setGoodsType(goodsType.getGoodsType());
		return goodsTypeVo;
	}
}
