package com.transporter.model;

public enum CouponDiscountType {
	FLAT, 
	PERCENTAGE
}
