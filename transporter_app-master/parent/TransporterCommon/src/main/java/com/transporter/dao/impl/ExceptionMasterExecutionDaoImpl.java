package com.transporter.dao.impl;

import org.springframework.stereotype.Repository;

import com.transporter.dao.ExceptionMasterExecutionDao;

/**
 * @author Devappa.Arali
 *
 */

@Repository
public class ExceptionMasterExecutionDaoImpl extends GenericDaoImpl
        implements ExceptionMasterExecutionDao {

}
