package com.transporter.service;

import com.transporter.vo.InternalUserDetailsVo;

/**
 * @author Devappa.Arali
 *
 */

public interface InternalUserService {

	String createInternalUser(InternalUserDetailsVo internalUserDetailsVo);

}
