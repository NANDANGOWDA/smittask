package com.transporter.service;

import java.util.List;

import com.transporter.vo.InternalUserRoleMasterVo;

/**
 * @author Devappa.Arali
 *
 */

public interface CommonService {

	List<InternalUserRoleMasterVo> getInternalUserRoles();

}
